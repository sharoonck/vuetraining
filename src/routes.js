import task1 from  './components/task1.vue'
import punchGame from  './components/punchGame.vue'
import converter from  './components/converter.vue'
import ipop_parent from  './components/ipop_parent.vue'
import colours_parent from  './components/colours_parent.vue'

export default [
    
    {path:'/task1',component: task1, name:"task1"},
    {path:'/colours_parents',component: colours_parent, name:"colours_parent"},
    {path:'/converter',component: converter, name:"converter"},
    {path:'/ipop_parent',component: ipop_parent, name:"ipop_parent"},
    {path:'/punchGame',component: punchGame, name:"punchGame"}
]
