class output {
    constructor (details){
        this.name=details.name;
        this.age=details.age;
        this.city=details.city;
        console.log('This is Printed from helper.js')
    }
    displayname(){
        console.log('Name : ',this.name)
    }
    displayage(){
        console.log(' Age : ',this.age)
    }
    displaycity(){
        console.log('City : ',this.city)
    }
}
export {output}

class seprator {
    constructor (value){
        this.value=value;
    }
    colourcode(){
        return this.value.split("-")[0];
    }
    Pincode(){
        return this.value.split("-")[1];
    }
   
}
export {seprator}
